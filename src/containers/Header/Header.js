import React, { Component } from 'react';
import './Header.css';
import Progress from '../../components/Progress/Progress';
import StorageService from '../../services/StorageService/StorageService';
import EditModalForm from '../../components/ModalForm/EditModalForm/EditModalForm';
import Attribute from '../../components/Attribute/Attribute';

class Header extends Component {

    state = {
        isOpenModalWindow: false,
        attrs: [
            {
                title: 'Название книги',
                value: '',
                multiple: false,
                edit: true,
                attrName: 'name'
            },
            {
                title: 'Авторы',
                value: '',
                multiple: false,
                edit: true,
                attrName: 'authors'
            },
            {
                title: 'Описание',
                value: '',
                multiple: false,
                edit: true,
                attrName: 'description'
            }
        ]
    };


    modalWindowHandler = () => {
        this.setState({ isOpenModalWindow: !this.state.isOpenModalWindow });
    }

    editValueHandler = (event, bookId, attrName) => {

    }

    render() {
        let createButtonRender;
        if (StorageService.enableEditMode) {
            createButtonRender =
                <div onClick={() => this.modalWindowHandler()} className="CreateBookButton">
                    <a>+</a>
                </div>;
        }
        let renderAttrs = this.state.attrs.map(attr => <Attribute title={attr.title} value={attr.value} multiple={attr.multiple} edit={attr.edit} changed={(event) => this.editValueHandler(event, -1, attr.attrName)}/>);

        let editModalForm = <EditModalForm
            title={'Создание новой книги'}
            onSubmit={() => alert('Книга создана(нет).')}
            onCancel={() => this.modalWindowHandler()}
            isOpen={this.state.isOpenModalWindow}
        >
            {renderAttrs}
        </EditModalForm>;

        return <div className="Header">
            <span>Библиотека МОУ ТСШ №9</span>
            <div className="Mini">г.Тирасполь
            </div>
            <div id="Progress">
                <Progress />
            </div>
            {createButtonRender}
            {editModalForm}
        </div>
    }
}

export default Header;