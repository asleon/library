import React, { Component } from 'react';
import './App.css';
import Body from './Body/Body';
import Header from './Header/Header';

class App extends Component {
  render() {
    return (
      <div className="App">
       <Header/>
       <Body bookListTitle="Список литературы"/>
      </div>
    );
  }
}

export default App;
