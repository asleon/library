import React, { Component } from 'react';
import './Body.css';
import Books from '../../components/Books/Books';
import BookList from '../../components/BookList/BookList';
import BookService from '../../services/BookService/BookService';
import StorageService from '../../services/StorageService/StorageService';
class Body extends Component {

  bookService = new BookService();

  state = {
    books: [],
    otherState: 'some other value'
  };

  downloadBookHandler = (bookIndex) => {
    console.log(`Скачивается книга с ${bookIndex+1}`);
    alert('Функция временно недоступна');
  }

  changedAttrHandler = (event, id, attrName) => {
    const bookIndex = this.state.books.findIndex(
      p => p.id === id
    );

    const book = { ...this.state.books[bookIndex] };

    if(attrName == 'name') {
      book.name = event.target.value;
    } else if(attrName == 'authors') {
      book.authors = event.target.value;
    } else if(attrName == 'description') {
      book.description = event.target.value;
    }

    const books = [...this.state.books];
    books[bookIndex] = book;

    this.setState({ books: books });
  }

  editBookHandler = (event, id) => {
    
    alert('Изменения сохранены(нет).');
  }

  toggleBookHandler = () => {
    this.bookService.getAllBooks().then(res => this.setState({ books: [ ... res] }));
    this.setState({ showBooks: !this.state.showBooks });
  }

  deleteBookHandler = (id) => {
    console.log(`id = ${id}`);
    const books = this.state.books.filter(book => book.id !== id);
    this.bookService.deleteBookById(id).then(res => alert((res == 200) ? `Книга успешно удалена.`: `Не удалось удалить книгу!`));
    console.log(books);
    this.setState({ books: books });
  }

  render() {
    let books = null;
    if (this.state.showBooks) {
      books = <div>
          <Books
            books={this.state.books}
            clicked={this.downloadBookHandler}
            edit={this.editBookHandler}
            deleted={this.deleteBookHandler}
          />
        </div>;
    }

    return (
      <div className="Body">
        <BookList
          title={this.props.bookListTitle}
          showBooks={this.state.showBooks}
          books={this.state.books}
          clicked={this.toggleBookHandler} />
        {books}
      </div>
    );
  }
}

export default Body;