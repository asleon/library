import React, { Component } from 'react';
import './Attribute.css';

class Attribute extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title,
            value: this.props.value,
            multiple: this.props.multiple,
            edit: this.props.edit
        };
    }

    changeValue = (event) => {
        let newValue = event.target.value;
        console.log(newValue);
        this.setState({value: newValue});
    }

    render() {
        let valueRender;
        if (!this.state.edit) {
            valueRender = this.state.value;
            if (this.state.multiple) {
                valueRender = this.state.value.map(item =>
                    <span className="Multiple-value">{item}</span>
                );
            }
        } else {
            valueRender = <input type="text" onChange={this.changeValue} value={this.state.value} />
            if (this.state.multiple) {
                valueRender = this.props.value.map(item =>
                    <input type="text" onChange={this.changeValue} value={this.state.value} />
                );
            }
        }

        return <div className="Attribute">
            <div className="Title">{this.state.title}</div>
            <div className="Value">{valueRender}</div>
        </div>
    }
}

export default Attribute;