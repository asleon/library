import React from 'react';
import ModalForm from '../ModalForm';
import PropTypes from 'prop-types';

import './EditModalForm.css';

const EditModalForm = ({
    title, isOpen, onCancel, onSubmit, children
}) => {
    return (
        <>
            {isOpen &&
                <ModalForm>
                    <div className="ModalOverlay">
                        <div className="ModalWindow">
                            <div className="ModalHeader">
                                <div className="ModalTitle">
                                    {title}
                                </div>
                                {/* <Icon name="names" onClick={onCancel} /> */}
                            </div>
                            <div className="ModalBody">
                                {children}
                            </div>
                            <div className="ModalFooter">
                                <button onClick={onSubmit}>Сохранить</button>
                                <button onClick={onCancel} invert>Отмена</button>
                            </div>
                        </div>
                    </div>
                </ModalForm >
            }
        </>
    );
};

EditModalForm.propTypes = {
    title: PropTypes.string,
    isOpen: PropTypes.bool,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    children: PropTypes.node
};

EditModalForm.defaultProps = {
    title: 'Modal title',
    isOpen: false,
    onCancel: () => {},
    onSubmit: () => {},
    children: null
};



export default EditModalForm;