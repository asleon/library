import React from 'react';
import './Progress.css';
import StorageService from '../../services/StorageService/StorageService';

const progress = (props) => {
    let value = StorageService.progressValue;
    if(props.value) {
        value = props.value;
    }
    return <progress className="Progress" value={ value / 100} />;
}

export default progress;