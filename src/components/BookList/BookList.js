import React from 'react';
import './BookList.css';

const bookList = (props) => {
    const classes = [];

    const style = {
        backgroundColor: 'orange',
        color: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '8px',
        cursor: 'pointer'
    };

    let filterType="";

    let buttonRender = <button
        style={style}
        onClick={props.clicked}>Показать книги по алфавиту</button>;

    if (props.showBooks) {
        buttonRender = <span></span>;
        filterType="отсортированный по названию книг";
    }

    classes.push('default');

    if (props.books.length <= 2) {
        classes.push('red');
    }

    if (props.books.length <= 1) {
        classes.push('bold');
    }

    return (
        <div className="BookList">
            <p>
                <h1 className={classes.join(' ')}>{props.title} {filterType}</h1>
            </p>
            {buttonRender}
        </div>
    );
};

export default bookList;