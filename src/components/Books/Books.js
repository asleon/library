import React from 'react';
import Book from './Book/Book';
import StorageService from '../../services/StorageService/StorageService';

const books = (props) => props.books.map((book, index) => {
    return <Book
        click={() => props.clicked(index)}
        edit={() => props.edit(book)}
        delete={() => props.deleted(book.id)}
        name={book.name}
        authors={book.authors}
        description={book.description}
        photoUrl={book.photoUrl}
        key={book.id}
        changed={(event) => props.changed(event, book.id)}
        enableEditMode={StorageService.enableEditMode} />
});


export default books;