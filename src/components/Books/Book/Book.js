import React from 'react';
import './Book.css';
import Attribute from '../../Attribute/Attribute';
import StorageService from '../../../services/StorageService/StorageService';

const book = (props) => {
    let attrs = [
        {
            title: 'Название книги',
            value: props.name,
            multiple: false,
            edit: props.enableEditMode,
            changed: (event) => props.changed(event, 'name' , props.key)
        },
        {
            title: 'Автор',
            value: props.authors,
            multiple: true,
            edit: props.enableEditMode,
            changed: (event) => props.changed(event, 'authors' , props.key)
        },
        {
            title: 'Описание',
            value: props.description,
            multiple: false,
            edit: props.enableEditMode,
            changed: (event) => props.changed(event, 'description' , props.key)
        }
    ];

    let attrsRender = attrs.map(attr =>
        <li className="Item-content">
            <Attribute title={attr.title}
                value={attr.value}
                multiple={attr.multiple}
                edit={attr.edit}
                changed={attr.changed} 
                />
        </li>
    );

    let renderActionsPanel;
    if (props.enableEditMode) {
        console.log('Enable!');
        renderActionsPanel = <p>
            <ul>
                <li>
                    <span className="ActionLabel">Действия:</span>
                </li>
                <li>
                    <button className="ActionButton" onClick={props.edit}>Сохранить</button>
                </li>
                <li>
                    <button className="ActionButton" onClick={props.delete}>Удалить</button>
                </li>
                <li>
                    {/* <button className="ActionButton" onClick={}>Отмена</button> */}
                </li>
            </ul>
            <hr />
        </p>;
    }

    return (
        <div className="Book">
            {renderActionsPanel}
            <ul>
                <li className="Item-content-extreme"><img src={props.photoUrl} /></li>
                {attrsRender}
                <li className="Item-content-extreme"><button className="AttrButton" onClick={props.click}>Скачать</button></li>
            </ul>
            <p>{props.children}</p>
        </div>
    );
}

export default book;