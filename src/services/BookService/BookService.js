import StorageService from '../StorageService/StorageService';
import ReactDOM from 'react-dom';
import React from 'react';
import Progress from '../../components/Progress/Progress';

class BookService { 

    sleep = (time) => {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    getBooks = async (url) => {
        // Шаг 1: начинаем загрузку fetch, получаем поток для чтения
        let response = await fetch(url);

        const reader = response.body.getReader();

        // Шаг 2: получаем длину содержимого ответа
        const contentLength = + 150;

        // Шаг 3: считываем данные:
        let receivedLength = 0; // количество байт, полученных на данный момент
        let chunks = []; // массив полученных двоичных фрагментов (составляющих тело ответа)
        while (true) {
            const { done, value } = await reader.read();
            if (done) {
                break;
            }

            chunks.push(value);
            receivedLength += value.length;
            this.loader(receivedLength, chunks);
        }

        // Шаг 4: соединим фрагменты в общий типизированный массив Uint8Array
        let chunksAll = new Uint8Array(receivedLength); // (4.1)
        let position = 0;
        for (let chunk of chunks) {
            chunksAll.set(chunk, position); // (4.2)
            position += chunk.length;
        }

        // Шаг 5: декодируем Uint8Array обратно в строку
        let result = new TextDecoder("utf-8").decode(chunksAll);

        // Готово!
        let obj = JSON.parse(result);
        let books = JSON.parse(obj.items);
        return books;
    }

    getAllBooks() {
        return this.getBooks(StorageService.URL_BOOKS);
    }

    loader = async (receivedLength, chunks) => {
        StorageService.progressValue = receivedLength;
        if (chunks.length == 1) {
            for (let i = receivedLength; i > 0; i -= 3) {
                ReactDOM.render(<Progress value={receivedLength / i} />, document.getElementById('Progress'));
                await this.sleep(1);
            }
        } else {
            ReactDOM.render(<Progress value={StorageService.progressValue} />, document.getElementById('Progress'));
        }
        StorageService.progressValue = 0;
        await this.sleep(250);
        ReactDOM.render(<Progress value={StorageService.progressValue} />, document.getElementById('Progress'));
    }

    deleteBookById(id) {
        return this.deleteBook(id);
    }

    // Action methods:
    async deleteBook(id) {
        
        let response = await fetch(`${StorageService.URL_BOOKS}/${id}`, {
            method: 'DELETE',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: {id: id}
          });

        return response.status;
    }

}

export default BookService;